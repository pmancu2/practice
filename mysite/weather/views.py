from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader
import pandas as pd
import requests
import datetime

def index(request):
    year = datetime.datetime.today().year
    years = range(year, year-20, -1)
    context = {
        'years': years,
    }
    return render(request, 'weather/index.html', context)
    #return HttpResponse(template.render(context, request))

def table(request):
     year_id=request.POST['date']
     dta = {"sid":"kbtr","sdate":str(year_id)+"0101","edate":str(year_id)+"1231","elems":"maxt,mint,pcpn","output":"json"}
     r=requests.post("http://data.rcc-acis.org/StnData", json=dta)
     s = pd.Series(r.json())
     #strip data of everything but essential data
     raw_json=s.get('data')
     #sort data into dateFrame
     cols=["date", "maxt", "mint", "pcpn"]
     df= pd.DataFrame(raw_json, columns=cols)
     #set date to Datetime object and index
     df.date = pd.to_datetime(df.date)
     df.index=df.date
     df=df.drop(columns='date')
     df2 = df
     #set remaining cols to numeric objects
     df.maxt = pd.to_numeric(df.maxt, errors="coerce")
     df.mint = pd.to_numeric(df.mint, errors="coerce")
     df.pcpn = pd.to_numeric(df.pcpn, errors="coerce")
     #resample objects based on months
     df2.maxt= df.maxt.resample('M').mean()
     df2.mint= df.mint.resample('M').mean()
     df2.pcpn= df.pcpn.resample('M').sum()
     df2=df2.resample('M').sum()
     df2.index= df2.index.strftime("%Y-%m")
     # monthData= df2.get(year_id)
     return HttpResponse(df2.to_html())
     # return render(request, 'weather/detail.html', {'monthData': df2.to_html()})
