from django.urls import path

from . import views

app_name = 'weather'
urlpatterns = [
    #ex: /weather/
    path('', views.index, name='index'),
    # ex: /weather/2017-02/
    path('table', views.table, name='table'),
]
