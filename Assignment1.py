import pandas as pd
import requests
import datetime

#r=requests.get("http://data.rcc-acis.org/StnData?sid=kbtr$sdate=20170101&edate=20171231&elems=maxt,mint,pcpn&output=json")
dta = {"sid":"kbtr","sdate":"20160101","edate":"20180101","elems":"maxt,mint,pcpn","output":"json"}
#r=requests.get("http://data.rcc-acis.org/StnData", params=dta)
r=requests.post("http://data.rcc-acis.org/StnData", json=dta)

s = pd.Series(r.json())
#strip data of everything but essential data
raw_json=s.get('data')
#sort data into dateFrame
cols=["date", "maxt", "mint", "pcpn"]
df= pd.DataFrame(raw_json, columns=cols)
#set date to Datetime object and index
df.date = pd.to_datetime(df.date)
df.index=df.date
df=df.drop(columns='date')
df2 = df

#set remaining cols to numeric objects
df.maxt = pd.to_numeric(df.maxt, errors="coerce")
df.mint = pd.to_numeric(df.mint, errors="coerce")
df.pcpn = pd.to_numeric(df.pcpn, errors="coerce")
#resample objects based on months
df2.maxt= df.maxt.resample('M').mean()
df2.mint= df.mint.resample('M').mean()
df2.pcpn= df.pcpn.resample('M').sum()
df2=df2.resample('M').sum()
df2.index= df2.index.strftime("%Y-%m")
print(df2)
f=open('2017Summary.txt', 'w')
print(df2, file=f)
