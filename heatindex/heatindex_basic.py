import requests
import json
import pprint
import io
url = 'http://hrly.lsu.edu/GetHrlyData/'
cstr = io.StringIO()

for i in range(2017, 2018, 2):
    params = {
        "order": "asc",
        "offset": 0,
        "limit": 50,
        "sdate": "{}010100".format(i),
        "edate": "{}010100".format(i + 2),
        "stime": "",
        "etime": "",
        "stn": "KBTR",
        "src": "",
        "interval": "1h",
        "elems": "dewpt,temp",
        "summary": "",
        "reduce": "",
        "where": ""
    }
    z = requests.post(url, data=json.dumps(params), headers={
                      'Content-type': 'application/json'})
    vals = z.json()
    print(vals.keys())
    dvals = vals['data']['values']
    print('number of values=', len(dvals['temp']))
    cstr.write ("%s,%s,%s\n" % ("Date","temp","dewpt"))

    for idx, val in enumerate(dvals['temp']):
        cstr.write("%s,%s,%s\n" % (dvals['time'][idx], dvals['temp'][
            idx], dvals['dewpt'][idx]))

with open('kbtr.csv', 'w') as fp:
    fp.write(cstr.getvalue())

# Start Code

# Imported the CSV, to read the data in.
import pandas as pd
df = pd.read_csv('kbtr.csv')
df.head()

# Created a new column called "heatIndex"
df['heatIndex'] = 'M'
df.head()

# Used the temperature and dewpoint to calculate the Relative Humidity (The temperature and dewpoint had to be converted to degrees Celsius).
# Once the RH was found, the heat index was found. Yet, there had to be a break because at certain temperature thresholds, the heat index equation is different.
# For example: if the temperature is below 80F, the reduced equation is used and above 80F the full equation is used.


for i in range(len(df)):
    if (df['temp'][i] != None and df['temp'][i] !='' and df['temp'][i] != 'None' and df['dewpt'][i] !=
        None and df['dewpt'][i] !='' and df['dewpt'][i] != 'None'):
        T = float(df['temp'][i])
        Tc = (5/9)*(T-32.0)
        D = float(df['dewpt'][i])
        Tdc = (5/9)*(D-32.0)
        Es=6.11*10.0**(7.5*Tc/(237.7+Tc))
        E=6.11*10.0**(7.5*Tdc/(237.7+Tdc))
        RH = (E/Es)*100
        if (T<80.0):
            df['heatIndex'][i] = 0.5 * (T + 61.0 + ((T-68.0)*1.2) + (RH*0.094))
        else:
            df['heatIndex'][i] = -42.379 + 2.04901523*T + 10.14333127*RH - .22475541*T*RH - .00683783*T*T - .05481717*RH*RH + .00122874*T*T*RH + .00085282*T*RH*RH - .00000199*T*T*RH*RH
    else:
        pass

# Printed the first 200 lines of the data for a test.
print(df.head(200))
df.to_csv('heat_Index.csv',index=None)
