import requests
import json
import csv
import io
import pandas as pd
import numpy as np
import argparse
import sys
from datetime import datetime

parser = argparse.ArgumentParser()
parser.add_argument("-s", "-station", help="enter station id as sting")
parser.add_argument("-sd", "-startDate", help="enter the start date (yyyymmdd)")
parser.add_argument("-ed", "-endDate", help="enter the end date (yyyymmdd)")
parser.add_argument("-hi", type=int, help="enter the highest heat index threshold in F")
parser.add_argument("-low", type=int, help="enter the lowest windchill threshold in F")
parser.add_argument("-t",
help="true/false, show table of threshold values or not", action="store_true")
parser.add_argument("-html", help="outputs as html", action="store_true")

args= parser.parse_args()

url = 'http://hrly.lsu.edu/GetHrlyData/'
cstr = io.StringIO()


params = {
    "order": "asc",
    "offset": 0,
    "limit": 50,
    "sdate": "{}00".format(args.sd),
    "edate": "{}00".format(args.ed),
    "stime": "",
    "etime": "",
    "stn": args.s,
    "src": "",
    "interval": "1h",
    "elems": "dewpt,temp,wind_speed",
    "summary": "",
    "reduce": "",
    "where": ""
}
z = requests.post(url, data=json.dumps(params), headers={
                  'Content-type': 'application/json'})

vals = z.json()
# print(vals)
dvals = vals['data']['values']
print('number of entries in date range=', len(dvals['temp']))
cstr.write("%s,%s,%s,%s\n" % ("Date", "Temp (F)", "Dew Point", "Wind Speed"))
for idx, val in enumerate(dvals['temp']):
    cstr.write("%s,%s,%s,%s\n" % (dvals['time'][idx], dvals['temp'][
        idx], dvals['dewpt'][idx], dvals['wind_speed'][idx]))

with open('kbtr.csv', 'w') as fp:
    fp.write(cstr.getvalue())

# Start Code

# Imported the CSV, to read the data in.

d = pd.read_csv('kbtr.csv')
df= d.set_index('Date', drop=True)
df.index= df.index.map(str)

# Removing missing values (Cleaning the data)

df.ix[:,0] = pd.to_numeric(df.ix[:,0], errors="coerce")
df.ix[:,1] = pd.to_numeric(df.ix[:,1], errors="coerce")
df.ix[:,2] = pd.to_numeric(df.ix[:,2], errors="coerce")

df.loc[:,'Wind Speed'] *= 1.151
# print(len(df))

def windchill(x,y):
    # 35.74 + 0.6215T - 35.75(V0.16) + 0.4275T(V0.16)
    wc= 35.74 + 0.6215 * x - 35.75 * (y**0.16) + 0.4275 * x*(y**0.16)
    return round(wc,1)

def relativehumidity(x):
    x = (5 / 9) * (float(x) - 32.0)
    return round((6.11 * 10.0**(7.5 * x / (237.7 + x))), 1)

# if the temperature is below 80F


def temp_(x, y):
    return round((0.5 * (x + 61.0 + ((x - 68.0) * 1.2) + (y * 0.094))), 1)

# if the temperature is above 80F


def temp(x, y):
    return round((-42.379 + 2.04901523 * x + 10.14333127 * y - .22475541 * x * y - .00683783 * x * x - .05481717 * y * y + .00122874 * x * x * y + .00085282 * x * y * y - .00000199 * x * x * y * y), 1)


df['RH'] = 100 * np.vectorize(relativehumidity)(df['Dew Point']) / np.vectorize(relativehumidity)(df['Temp (F)'])
df.RH = df.RH.round(1)
# print (df.head())

# Used the temperature and dewpoint to calculate the Relative Humidity (The temperature and dewpoint had to be converted to degrees Celsius).
# Once the RH was found, the heat index was found. Yet, there had to be a break because at certain temperature thresholds, the heat index equation is different.

df['Heat Index'] = [np.vectorize(temp_)(float(a), float(b)) if float(a) < 80.0 else np.vectorize(temp)(float(a), float(b)) for a, b in zip(df.iloc[:, 0], df.iloc[:, 3])]
# print (df.head())

df['Wind Chill'] = np.vectorize(windchill)(df['Temp (F)'], df['Wind Speed'])

# Printed the first 200 lines of the da ta for a test.
# print(df.head(200))
if(args.t or args.html):
    df2=df[df['Heat Index'] > args.hi]
    if(args.html):
        print(df2.to_html())
    else:
        print(df2)

startDate=datetime.strptime(args.sd, '%Y%m%d').strftime('%b %d, %Y')
endDate=datetime.strptime(args.ed, '%Y%m%d').strftime('%b %d, %Y')

print("==============================================================================================")
if args.hi != None:
    print("Number of hours with Heat Index above {}F = {} \nfor date range: {} to {}".format(args.hi, (df['Heat Index']> args.hi).sum(), startDate, endDate))

if args.low != None:
    print("Number of hours with Wind Chill below {}F = {} \nfor date range: {} to {}".format(args.low, (df['Wind Chill']<args.low).sum(), startDate, endDate))
    # df.to_csv('heat_Index.csv', index=None)
