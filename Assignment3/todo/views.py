import tornado.ioloop
import tornado.web

class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.write("Hello, world")

def make_app():
    return tornado.web.Application([
        (r"/", MainHandler),
    ])

if __name__ == "__main__":
    app = make_app()
    app.listen(8888)
    tornado.ioloop.IOLoop.current().start()



# from tornado.web import RequestHandler
# import requests
# import json
# import csv
# import io
# import pandas as pd
# import numpy as np
# import argparse
# import sys
# from datetime import datetime
#
#
# url = 'http://hrly.lsu.edu/GetHrlyData/'
# cstr = io.StringIO()
#
#
# params = {
#     "order": "asc",
#     "offset": 0,
#     "limit": 50,
#     "sdate": "{}00".format(20180101),
#     "edate": "{}00".format(20180202),
#     "stime": "",
#     "etime": "",
#     "stn": "KBTR",
#     "src": "",
#     "interval": "1h",
#     "elems": "dewpt,temp,wind_speed",
#     "summary": "",
#     "reduce": "",
#     "where": ""
# }
# z = requests.post(url, data=json.dumps(params), headers={
#                   'Content-type': 'application/json'})
#
# vals = z.json()
#
# class HelloWorld(RequestHandler):
#     """Print 'Hello,' as the response body."""
#
#     def get(self):
#         """Handle a GET request for saying Hello World!."""
#         self.write("Hi World")
