import tornado.ioloop
import tornado.web as web
import os.path
import pandas as pd
import requests
import datetime

url='http://storm2.lsu.edu:8076/getLSUWeather'
hrs =120
param= '?date='
start=datetime.datetime.now()-datetime.timedelta(hours=hrs)
# start-=(start%1800)
param+=str(start)
url+=param
print(start)
120
r=requests.get(url);
df = pd.DataFrame.from_dict(r.json(), orient='columns')
df['wind_chill'] = df.apply(lambda row: (35.74 + .6215 * row['temp_out'] - 35.75 * row['max_wind_speed']**.16 + .4275 *row['temp_out'] * row['max_wind_speed']**.16
                                               if row['max_wind_speed']>0
                                               else row['temp_out']),
                                   axis=1)
df['date_time'] = pd.to_datetime(df['date_time'])
prevDate=df.get('date_time')[0]-datetime.timedelta(minutes=30)
count=0
for i in range(len(df)):
    prevDate=prevDate+datetime.timedelta(minutes=30)
    date=df.get('date_time')[i]
    while prevDate != date:
        print(str(date)+"!="+str(prevDate))
        # df.loc[len(df)]=['', prevDate, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '']
        prevDate=prevDate+datetime.timedelta(minutes=30)
snapshot= df.ix[-1:].to_dict('list')
df=df.set_index('date_time')
df=df.sort_index()
# print(df.count())

dirs=["N", "NNE", "NE", "ENE", "E", "ESE", "SE", "SSE", "S", "SSW", "SW", "WSW", "W", "WNW", "NW", "NWN"]
dirData=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
dir=df['max_wind_dir'].value_counts()

count=0
for d in dirs:
    if d in dir:
        dirData[count]= dir[d]
    count +=1

print(dirs)
print(dirData)


class MainHandler(web.RequestHandler):
    def get(self):
        curTemp=df.ix[-1:13]
        self.render('weatherTemplate.html', title='My title',timeFrame=hrs, dirData= dirData, data=snapshot, temp=df[['max_wind_dir','max_wind_speed','temp_out', 'heat_index', 'wind_chill', 'dew_pt', 'pressure_bar', 'out_hum', 'rain_rate']].to_dict('list'))

# app = web.Application([
#     (r'/', MainHandler),
#     (r'/js/(.*)', web.StaticFileHandler, {'path': './js'}),
#     (r'/css/(.*)', web.StaticFileHandler, {'path': './css'}),
#     (r'/fonts/(.*)', web.StaticFileHandler, {'path': './fonts'}),
# ],)

settings = {
   'static_path': os.path.join(os.path.dirname(__file__), 'static'),
}

app = tornado.web.Application({
   (r'^/$', MainHandler),
}, **settings)


if __name__ == "__main__":
    app.listen(8888)
    tornado.ioloop.IOLoop.current().start()
