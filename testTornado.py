import requests
import tornado.ioloop
import tornado.web
from tornado.web import RequestHandler
from tornado.httpclient import AsyncHTTPClient
import json
import csv
import io
import os
from tornado_sqlalchemy import make_session_factory


WeatherUrl = 'http://hrly.lsu.edu/GetHrlyData/'
cstr = io.StringIO()


params = {
    "order": "asc",
    "offset": 0,
    "limit": 50,
    "sdate": "{}00".format(20180101),
    "edate": "{}00".format(20180202),
    "stime": "",
    "etime": "",
    "stn": "KBTR",
    "src": "",
    "interval": "1h",
    "elems": "dewpt,temp,wind_speed",
    "summary": "",
    "reduce": "",
    "where": ""
}
z = requests.post(WeatherUrl, data=json.dumps(params), headers={
                  'Content-type': 'application/json'})

vals = z.json()

class DataPrinter(tornado.web.RequestHandler):
    def get(self):
        # msg=asynchronous_fetch(WeatherUrl)
        self.write(vals)

class InfoView(RequestHandler):
    SUPPORTED_METHODS = ['GET']

    def set_default_headers(self):
        self.set_header("Content-Type", 'application/json; charset="utf-8"')

    def get(self):
        routes = {
            'info' :'POST /GetHrlyData',
        }
        self.write(json.dumps(routes))

async def asynchronous_fetch(url):
    http_client = AsyncHTTPClient()
    response = await http_client.fetch(url)
    return response.body


def make_app():
    factory = make_session_factory(os.environ.get('http://hrly.lsu.edu',''))
    return tornado.web.Application([
        (r"/", InfoView),
    ],
        session_factory=factory
    )

if __name__ == "__main__":
    app = make_app()
    app.listen(8888)
    tornado.ioloop.IOLoop.current().start()
