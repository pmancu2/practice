from django.core.management.base import BaseCommand, CommandError
from MonthlyData.models import MonthlyAvg
from datetime import datetime
from django.utils import timezone

class Command(BaseCommand):
    def handle(self, *args, **options):
        f=open('/home/patrick/Documents/Test/practice/2017Summary.txt')
        f.readline()
        for line in f:
            line_data=line.split()
            month_obj=datetime.strptime(line_data[0],"%Y-%m")
            final_month_obj=timezone.make_aware(month_obj)
            print(final_month_obj)
            newMonthlyData= MonthlyAvg(month=final_month_obj, max_t=line_data[1], min_t=line_data[2], pcp_n=line_data[3])
            try:
                newMonthlyData.save()
                print("added:",line)
            except:
                print("there was a problem....")
