from django.apps import AppConfig

class MonthlyDataConfig(AppConfig):
    name = 'MonthlyData'
