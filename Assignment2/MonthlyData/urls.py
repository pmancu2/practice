from django.urls import path
from . import views
from MonthlyData.views import MonthlyAvg

urlpatterns = [
    path('', views.index, name='index'),
    path('table/', views.table, name='table'),
    path('<int:question_id>/monthReq/', views.monthReq, name='monthReq'),
    path('<int:question_id>/results/', views.results, name='results'),
]
