from django.shortcuts import get_object_or_404, render
from django.template import loader
from django_tables2 import RequestConfig
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from .models import MonthlyAvg
from .tables import MonthlyTable

def index(request):
    template=loader.get_template('MonthlyData/table.html')
    context= {'MonthlyAvgs': MonthlyAvg.objects.all()}
    return HttpResponse(template.render(context, request))
    #return HttpResponse("Hello, world. You're at the MonthlyData index.")

def table(request):
    table= MonthlyTable(MonthlyAvg.objects.all())
    RequestConfig(request).configure(table)
    template=loader.get_template('MonthlyData/table.html')
    context= {'table': table}
    #return render(request, "MonthlyAvg/table.html", {'table': table})
    return HttpResponse(template.render(context, request))

def monthReq(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    try:
        selected_choice = question.choice_set.get(pk=request.POST['choice'])
    except(KeyError, Choice.DoesNotExist):
        #redisplay the question voting form
        return render(request, 'MonthlyData/monthReq.html', {
            'question':question,
            'error_message': "You didn't select a choice.",
        })
    else:
        selected_choice.votes +=1
        selected_choice.save()
        return HttpResponseRedirect(reverse('pools:results', args=(question_id)))

def results(request, question_id):
    question= get_object_or_404(Question, pk=question_id)
    return render(request, 'MonthlyData/results.html', {'question':question})
    

# Create your views here.
