from django.contrib import admin

# Register your models here.
from .models import MonthlyAvg

admin.site.register(MonthlyAvg)
