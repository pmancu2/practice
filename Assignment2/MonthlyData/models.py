from django.db import models

# Create your models here.
class MonthlyAvg(models.Model):
        month=models.DateTimeField(primary_key=True)
        max_t= models.DecimalField(max_digits=9,decimal_places=6)
        min_t= models.DecimalField(max_digits=9,decimal_places=6)
        pcp_n= models.DecimalField(max_digits=4,decimal_places=2)
