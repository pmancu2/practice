import django_tables2 as tables
from .models import MonthlyAvg

class MonthlyTable(tables.Table):
    class Meta:
        model = MonthlyAvg
        template_name = 'MonthlyData/table.html'
